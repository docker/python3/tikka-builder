FROM python:3.7-slim-bullseye

# Install system requirements
RUN apt update && \
apt install --yes curl libsodium23 make gettext binutils zip git libglib2.0-0 libgl1 gcc\
    xvfb \
    libfontconfig1 libxcb-util1 libxcb-xinerama0 libxcb1 libdbus-1-3 \
    libxkbcommon0 libxkbcommon-x11-0 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 \
    libxcb-render0 libxcb-render-util0 libxcb-shape0 && \
rm -rf /var/lib/apt/lists

# Install Poetry
ENV POETRY_HOME="/opt/poetry"
ENV PATH="$POETRY_HOME/bin:$PATH"
RUN curl -sSL https://install.python-poetry.org | python - && poetry config virtualenvs.create false

# Install main big dependency
RUN pip install -U PyQt5=="5.15.9"

# Set X display port
ENV DISPLAY :99

# Add virtual X buffer start script
ADD startx.sh /startx.sh
RUN chmod u+x /startx.sh
