## Tikka builder container

The goal of this repository is to provide a container to build and run tests for pyqt5 application like Tikka client.

### Build

To build the image, type:

    docker build -t tikka-builder:latest .

### Run container

    docker run -ti --rm tikka-builder:latest bash

### Run tests

In the container, install dependencies and build app:

    git clone https://git.duniter.org/clients/python/tikka.git
    cd tikka
    poetry install --no-root
    bin/convert_resources.sh
    make i18n_build

To run a Qt application in the container, you need to start the X server by running the `startx.sh` script:

    /startx.sh

Then:

    bin/tests.sh

